zebra-zpl
=========

This library will help you to quickly generate ZPL code for your Zebra printer.

Library support only most commons ZPL commons (Text, Field Block, Code bar 39)

For the moment, library was tested on Zebra 200 dpi.

But you can insert natif zpl at all times.

You can also fork this project and share code if you make this library better.

Link to some zpl viewer: http://labelary.com/viewer.html

Feel free to use ZPL designer tools to speed up your layout design.

Hello World
=================


<!--
Add dependy in your pom

  	  <dependency>
	      <groupId>fr.w3blog</groupId>
	      <artifactId>zebra-zpl</artifactId>
	      <version>0.0.1</version>
	      <scope>compile</scope>
	    </dependency>
-->

Download the project and build jar. (pom is not available at the moment)

First code


		ZebraLabel zebraLabel = new ZebraLabel(912, 912);
		zebraLabel.setDefaultZebraFont(ZebraFont.ZEBRA_ZERO);

		zebraLabel.addElement(new ZebraText(10, 84, "Product:", 14));
		zebraLabel.addElement(new ZebraText(395, 85, "Camera", 14));

		zebraLabel.addElement(new ZebraText(10, 161, "CA201212AA", 14));

		//Add Code Bar 39
		zebraLabel.addElement(new ZebraBarCode39(10, 297, "CA201212AA", 118, 2, 2));

		zebraLabel.addElement(new ZebraText(10, 365, "Qté:", 11));
		zebraLabel.addElement(new ZebraText(180, 365, "3", 11));
		zebraLabel.addElement(new ZebraText(317, 365, "QA", 11));

		zebraLabel.addElement(new ZebraText(10, 520, "Ref log:", 11));
		zebraLabel.addElement(new ZebraText(180, 520, "0035", 11));
		zebraLabel.addElement(new ZebraText(10, 596, "Ref client:", 11));
		zebraLabel.addElement(new ZebraText(180, 599, "1234", 11));

Get Zpl Code

    zebraLabel.getZplCode();
		
Print

    ZebraUtils.printZpl(zebraLabel, ip, port);
  

Native code
=================

If you need to customise your label

zebraLabel.addElement(new ZebraNativeZpl("^KD0\n"));

You can also use usefull fonction ZplUtils.zplCommand to generate a zpl command (with many variables)

  ZplUtils.zplCommand("A", "0", "R");
  //will return ^A,0,R

Auto line spacing
=================

Auto adjusts the position of the next element based on the previous element.

Works well for text element only.

Terminology

1. Linespacing is the space in pixel between the next text element
2. Gap is the number of text element to follow until it goes to a new line.
3. StartingPositionX is the starting position X of the next text element
4. StartingPositionY is the starting position Y of the next textelement

Note: Starting positions are optional. If left empty, the last text element position is used.

    zebraLabel.setAutoLineSpacing(linespacing, gap, startingPostionX, StartingPositionY)
    zebraLabel.setAutoLineSpacing(linespacing, gap)
    
The following is an example of auto line spacing starting at location (10,10)

    zebraLabel.setAutoLineSpacing(50, 2, 10, 10);
    zebraLabel.addElement(new ZebraText("First Name: "));
    zebraLabel.addElement(new ZebraAFontElement(ZebraFont.ZEBRA_ZERO, 40,40));
    zebraLabel.addElement(new ZebraText("John"));
    
    zebraLabel.addElement(new ZebraText("Last Name: "));
    zebraLabel.addElement(new ZebraText("Smith"));
    zebraLabel.setAutoLineSpacingOff();

Output:

    First Name: John
    Last Name: Smith
    
The following is an example of auto line spacing starting at location (10,10)

    zebraLabel.addElement(new ZebraText(10, 10,"What is your name? "));
    
    zebraLabel.setAutoLineSpacing(50, 2);
    zebraLabel.addElement(new ZebraText("First Name: "));
    zebraLabel.addElement(new ZebraAFontElement(ZebraFont.ZEBRA_ZERO, 40,40));
    zebraLabel.addElement(new ZebraText("John"));
    
    zebraLabel.addElement(new ZebraText("Last Name: "));
    zebraLabel.addElement(new ZebraText("Smith"));
    zebraLabel.setAutoLineSpacingOff();

Output:

    What is your name?
    First Name: John
    Last Name: Smith



Other documentation
=================

Documentation about ZPL could be find here
http://www.tracerplus.com/kb/Manuals/ZPL_Vol1.pdf

http://www.tracerplus.com/kb/Manuals/ZPL_Vol2.pdf
