package fr.w3blog.zpl.model.element;

import junit.framework.TestCase;

public class ZebraFieldBlockTest extends TestCase {

//    public void testContainerZplOutput() {
//        ZebraContainer container = new ZebraContainer(10,10,760, 10, 10, 'l', 40, "Arr. Date:");
//        assertEquals("^FT10,10^FB760,10,10,l,40^FH\\^FDArr. Date:^FS\n", container.getZplCode(null));
//    }

    public void testContainerWithoutPositionZplOutput() {
        ZebraContainer container = new ZebraContainer("Arr. Date:", 'c', 11);
        assertEquals("^FT^FB760,10,10,l,40^FH\\^FDArr. Date:^FS\n", container.getZplCode(null));
    }
}
