package fr.w3blog.zpl.model.element;

import fr.w3blog.zpl.constant.ZebraFont;
import fr.w3blog.zpl.model.ZebraLabel;
import junit.framework.TestCase;

import java.util.List;
import java.util.Map;

/**
 * Created by MissFresh on 2017-07-14.
 */
public class MissfreshLabelTest extends TestCase {

    public void testMissFreshLabel(){
        ZebraLabel zebraLabel = new ZebraLabel(800, 1600);
        zebraLabel.setDefaultZebraFont(ZebraFont.ZEBRA_ZERO);

        zebraLabel.setLabelHome(20,20);

        zebraLabel.addElement(new ZebraGraficBox(0,0, 760, 1560 , 5,"B"));


        zebraLabel.addElement(new ZebraText(20,60,"Arr. Date: ", 11));
        zebraLabel.addElement(new ZebraText("2017-03-27", ZebraFont.ZEBRA_ZERO, 11));
        zebraLabel.addElement(new ZebraText(" - ", ZebraFont.ZEBRA_ZERO, 11));
        zebraLabel.addElement(new ZebraText("Monday", ZebraFont.ZEBRA_ZERO, 11));

        zebraLabel.addElement(new ZebraText(20,110,"Heure / Time: ", 11));
        zebraLabel.addElement(new ZebraText("12:00AM - 12:00PM", ZebraFont.ZEBRA_ZERO, 11));
        zebraLabel.addElement(new ZebraContainer(590, 90, 200, 1, 0, 'c', 0, "FN", ZebraFont.ZEBRA_ZERO, 18));
        zebraLabel.addElement(new ZebraContainer(0, 190, 760, 1, 0, 'c', 0, "589045293229", ZebraFont.ZEBRA_ZERO, 15));

        zebraLabel.addElement(new ZebraGraficBox(0,130, 760, 80 , 5,"B"));


        //Shipping information
        zebraLabel.addElement(new ZebraText(20,260,"EXPEDIER A / SHIP TO: ", 11));

        zebraLabel.addElement(new ZebraText(20,310,"Nom / Name: ", 11));
        zebraLabel.addElement(new ZebraText("Jimmy Riccio", 11));

        zebraLabel.addElement(new ZebraText(20,360,"Adresse / Address: ", 11));
        zebraLabel.addElement(new ZebraText(20,410,"8479", 11));
        zebraLabel.addElement(new ZebraText(" ", 11));
        zebraLabel.addElement(new ZebraText("12 Avenue", 11));

        zebraLabel.addElement(new ZebraText(20,460,"Ville / City: ", 11));
        zebraLabel.addElement(new ZebraText("Montreal", 11));

        zebraLabel.addElement(new ZebraText(20,510,"Province: ", 11));
        zebraLabel.addElement(new ZebraText("QC", 11));

        zebraLabel.addElement(new ZebraText(20,570,"Code postal / Postal Code: ", 11));
        zebraLabel.addElement(new ZebraText("H1Z 3H9", 11));

        zebraLabel.addElement(new ZebraText(20,610,"Tel. / Tel.: ", 11));
        zebraLabel.addElement(new ZebraText("+1 (514) 533-9642", 11));

        zebraLabel.addElement(new ZebraText(20,700,"Instructions: ", 11));
        zebraLabel.addElement(new ZebraContainer(20, 870, 740, 4, 8, 'l', 0, "Lorem ipsum dolor sit amet, consectetur adipiscing elit Lorem ipsum dolor sit amet, consectetur adipiscing elit", ZebraFont.ZEBRA_ZERO, 10));

        zebraLabel.addElement(new ZebraContainer(20,960, 400, 1, 0, 'l', 0, "CANADA POST", ZebraFont.ZEBRA_ZERO, 12));
        zebraLabel.addElement(new ZebraContainer(330,960, 400, 1, 0, 'r', 0, "WED-MON 0", ZebraFont.ZEBRA_ZERO, 15));
        zebraLabel.addElement(new ZebraGraficBox(0,900, 760, 90 , 5,"B"));


        //End Shipping Information


        //Recipes
//        StringBuilder perishableRecipeContent = new StringBuilder();
//        Map<short, List<short>> portionsByPerishableRecipeId = request.getPortionsByPerishableRecipeId()
//
//        portionsByPerishableRecipeId.forEach((k,v)-> {
//            recipeContent.append( k + "P " );
//                v.forEach(recipeId-> {
//                    int limit = 6;
//                    int count = 0;
//                    if(count<6) {
//                        recipeContent.append(recipeId + " | ");
//                        count++;
//                    } else {
//                        recipeContent.append("\&");
//                        recipeContent.append(k+"P ");
//                        count = 0;
//                    }
//                });
//        });
//
//        zebraLabel.addElement(new ZebraContainer(300,1000, 400, 1, 0, 'r', 0, perishableRecipeContent.toString(), ZebraFont.ZEBRA_ZERO, 11));
//
//        StringBuilder nonPerishableRecipeContent = new StringBuilder();
//        Map<short, List<short>> portionsByNonPerishableRecipeId = request.getPortionsByNonPerishableRecipeId()
//
//        nonPortionsByPerishableRecipeId.forEach((k,v)-> {
//            recipeContent.append( k + "x " );
//            v.forEach(recipeId-> {
//                int limit = 6;
//                int count = 0;
//                if(count<6) {
//                    recipeContent.append(recipeId + " | ");
//                    count++;
//                } else {
//                    recipeContent.append("\&");
//                    recipeContent.append(k+"x ");
//                    count = 0;
//                }
//            });
//        });
//
//        zebraLabel.addElement(new ZebraContainer(20,1200, 400, 1, 0, 'r', 0, nonPerishableRecipeContent.toString(), ZebraFont.ZEBRA_ZERO, 11));
//
//        //Box types
//        zebraLabel.addElement(new ZebraContainer(20,1300, 400, 1, 0, 'r', 0, "BT", ZebraFont.ZEBRA_ZERO, 11));
//
//        //TransitBox
//        zebraLabel.addElement(new ZebraContainer(20,1400, 400, 1, 0, 'r', 0, "TB", ZebraFont.ZEBRA_ZERO, 11));
//
//        StringBuilder transitContent = new StringBuilder();
//        Map<short, List<short>> transitBoxes = request.transitBoxes()
//
//        request.getTransitBoxes().forEach((k,v)-> {
//            recipeContent.append( "#" + k + " ");
//            v.forEach(recipeId-> {
//                int limit = 6;
//                int count = 0;
//                if(count<6) {
//                    recipeContent.append(recipeId + " | ");
//                    count++;
//                } else {
//                    recipeContent.append("\&");
//                    recipeContent.append(k+"x ");
//                    count = 0;
//                }
//            });
//        });
//
//        zebraLabel.addElement(new ZebraContainer(20,1450, 400, 1, 0, 'r', 0, transitContent.toString(), ZebraFont.ZEBRA_ZERO, 11));


        System.out.println(zebraLabel.getZplCode());

    }
}
