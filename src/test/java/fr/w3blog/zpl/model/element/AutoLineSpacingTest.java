package fr.w3blog.zpl.model.element;

import fr.w3blog.zpl.constant.ZebraFont;
import fr.w3blog.zpl.model.ZebraLabel;
import junit.framework.TestCase;

/**
 * Created by MissFresh on 2017-07-14.
 */
public class AutoLineSpacingTest extends TestCase {

    public void testAutoLineSpacingLabel(){
        ZebraLabel zebraLabel = new ZebraLabel(800, 1600);

        //Shipping information
//        zebraLabel.addElement(new ZebraText(20,280,"EXPEDIER A / SHIP TO: ", 11));
//
//        zebraLabel.setAutoLineSpacing(50,2);
//        zebraLabel.addElement(new ZebraText("Nom / Name: ", 11));
//        zebraLabel.addElement(new ZebraText("Jimmy Riccio", 11));
//
//        zebraLabel.addElement(new ZebraText("Ville / City: ", 11));
//        zebraLabel.addElement(new ZebraText("Montreal", 11));
//
//        zebraLabel.addElement(new ZebraText("Province: ", 11));
//        zebraLabel.addElement(new ZebraText("QC", 11));
//        zebraLabel.setAutoLineSpacingOff();

        zebraLabel.setAutoLineSpacing(50,2, 20, 800);
        zebraLabel.addElement(new ZebraText("Province: "));
        zebraLabel.addElement(new ZebraAFontElement(ZebraFont.ZEBRA_ZERO,30,32));
        zebraLabel.addElement(new ZebraText("QC"));
        zebraLabel.addElement(new ZebraText("Province: "));
        zebraLabel.addElement(new ZebraAFontElement(ZebraFont.ZEBRA_ZERO,30,32));
        zebraLabel.addElement(new ZebraText("Country: Canada"));
        zebraLabel.setAutoLineSpacingOff();

        assertEquals("^XA\n" +
                "^MMT\n" +
                "^PW800\n" +
                "^LL1600\n" +
                "^FT20,800^FH\\^FDProvince: ^FS\n" +
                "^A0N,30,32\n" +
                "^FT^FH\\^FDQC^FS\n" +
                "^FT20,850^FH\\^FDProvince: ^FS\n" +
                "^A0N,30,32\n" +
                "^FT^FH\\^FDCountry: Canada^FS\n" +
                "^XZ\n", zebraLabel.getZplCode());

        System.out.println(zebraLabel.getZplCode());

    }
}
