package fr.w3blog.zpl.model.element;

import fr.w3blog.zpl.constant.ZebraFont;
import fr.w3blog.zpl.constant.ZebraRotation;
import fr.w3blog.zpl.model.PrinterOptions;
import fr.w3blog.zpl.model.ZebraElement;
import fr.w3blog.zpl.utils.ZplUtils;

import java.awt.*;

import static java.util.Objects.isNull;

/**
 * Zebra element to add Text to specified position.
 * 
 * @author ttropard
 * 
 */
public class ZebraContainer extends ZebraText {

	Integer width = null;
	Integer maxLines = null;
	Integer lineSpacing = null;
	char alignment;
	Integer indent = null;

	public ZebraContainer(String text) {
		this.text = text;
	}

	public ZebraContainer(String text, char alignment) {
		this.text = text;
		this.alignment = alignment;
	}

	public ZebraContainer(String text, char alignment, int fontSize) {
		this.text = text;
		this.alignment = alignment;
	}

	public ZebraContainer(String text, char alignment, ZebraFont zebraFont, int fontSize) {
		this.text = text;
		this.alignment = alignment;
	}

	public ZebraContainer(String text, int fontSize) {
		this.fontSize = fontSize;
		this.text = text;
	}

	public ZebraContainer(String text, ZebraFont zebraFont, int fontSize) {
		this.zebraFont = zebraFont;
		this.fontSize = fontSize;
		this.text = text;
	}

	public ZebraContainer(String text, ZebraFont zebraFont, int fontSize, ZebraRotation zebraRotation) {
		this.zebraFont = zebraFont;
		this.zebraRotation = zebraRotation;
		this.fontSize = fontSize;
		this.text = text;
	}

	public ZebraContainer(int positionX, int positionY, String text) {
		this.text = text;
		this.positionX = positionX;
		this.positionY = positionY;
	}

	public ZebraContainer(int positionX, int positionY, int width, int maxLines, int lineSpacing, char alignment, int indent, String text) {
		this.text = text;
		this.positionX = positionX;
		this.positionY = positionY;

		this.width = width;
		this.maxLines = maxLines;
		this.lineSpacing = lineSpacing;
		this.alignment = alignment;
		this.indent = indent;
	}

	public ZebraContainer(int positionX, int positionY, String text, int fontSize) {
		this.fontSize = fontSize;
		this.text = text;
		this.positionX = positionX;
		this.positionY = positionY;
	}

	public ZebraContainer(int positionX, int positionY, String text, ZebraFont zebraFont, int fontSize, ZebraRotation zebraRotation) {
		this.zebraFont = zebraFont;
		this.fontSize = fontSize;
		this.zebraRotation = zebraRotation;
		this.text = text;
		this.positionX = positionX;
		this.positionY = positionY;
	}

	public ZebraContainer(int positionX, int positionY, int width, int maxLines, int lineSpacing, char alignment, int indent, String text, ZebraFont zebraFont, int fontSize) {
		this.zebraFont = zebraFont;
		this.fontSize = fontSize;
		this.text = text;
		this.positionX = positionX;
		this.positionY = positionY;

		this.width = width;
		this.maxLines = maxLines;
		this.lineSpacing = lineSpacing;
		this.alignment = alignment;
		this.indent = indent;
	}

	/* (non-Javadoc)
	 * @see fr.w3blog.zpl.model.element.ZebraElement#getZplCode(fr.w3blog.zpl.model.PrinterOptions)
	 */
	@Override
	public String getZplCode(PrinterOptions printerOptions) {

		StringBuffer zpl = new StringBuffer();
		zpl.append(this.getZplCodePosition());

		if (fontSize != null && zebraFont != null) {
			//This element has specified size and font
			Integer[] dimension = ZplUtils.extractDotsFromFont(zebraFont, fontSize, printerOptions.getZebraPPP());
			zpl.append(ZplUtils.zplCommand("A", zebraFont.getLetter() + zebraRotation.getLetter(), dimension[0], dimension[1]));
		} else if (fontSize != null && printerOptions.getDefaultZebraFont() != null) {
			//This element has specified size, but with default font
			Integer[] dimension = ZplUtils.extractDotsFromFont(printerOptions.getDefaultZebraFont(), fontSize, printerOptions.getZebraPPP());
			zpl.append(ZplUtils.zplCommand("A", printerOptions.getDefaultZebraFont().getLetter() + zebraRotation.getLetter(), dimension[0], dimension[1]));
		}

		//Field Block defaults


		zpl.append( ZplUtils.zplCommand("FB",Integer.toString(width), Integer.toString(maxLines), Integer.toString(lineSpacing), alignment , Integer.toString(indent) ) );

		zpl.append("^FH\\^FD");//We allow hexadecimal and start element
		zpl.append(ZplUtils.convertAccentToZplAsciiHexa(text));
		zpl.append(ZplUtils.zplCommandSautLigne("FS"));

		return zpl.toString();
	}

	/**
	 * Used to draw label preview.
	 * This method should be overloader by child class.
	 * 
	 * Default draw a rectangle
	 * 
	 * @param graphic
	 */
	public void drawPreviewGraphic(PrinterOptions printerOptions, Graphics2D graphic) {
		if (defaultDrawGraphic) {
			int top = 0;
			int left = 0;
			if (positionX != null) {
				left = ZplUtils.convertPointInPixel(positionX);
			}
			if (positionY != null) {
				top = ZplUtils.convertPointInPixel(positionY);
			}

			Font font = null;

			if (fontSize != null && zebraFont != null) {
				//This element has specified size and font
				Integer[] dimension = ZplUtils.extractDotsFromFont(printerOptions.getDefaultZebraFont(), fontSize, printerOptions.getZebraPPP());

				font = new Font(ZebraFont.findBestEquivalentFontForPreview(zebraFont), Font.BOLD, dimension[0]);
			} else if (fontSize != null && printerOptions.getDefaultZebraFont() != null) {
				//This element has specified size, but with default font
				Integer[] dimensionPoint = ZplUtils.extractDotsFromFont(printerOptions.getDefaultZebraFont(), fontSize, printerOptions.getZebraPPP());
				font = new Font(ZebraFont.findBestEquivalentFontForPreview(printerOptions.getDefaultZebraFont()), Font.BOLD, Math.round(dimensionPoint[0] / 1.33F));
			} else {
				//Default font on Printer Zebra
				Integer[] dimensionPoint = ZplUtils.extractDotsFromFont(printerOptions.getDefaultZebraFont(), 15, printerOptions.getZebraPPP());

				font = new Font(ZebraFont.findBestEquivalentFontForPreview(ZebraFont.ZEBRA_A), Font.BOLD, dimensionPoint[0]);
			}
			drawTopString(graphic, font, text, left, top);
		}
	}

	/**
	 * Function used by child class if you want to set position before draw your element.
	 *
	 * @return
	 */
	protected String getZplCodePosition() {

		StringBuffer zpl = new StringBuffer("");
		if (positionX != null && positionY != null) {
			zpl.append(ZplUtils.zplCommand("FO", positionX, positionY));
		} else if (isNull(positionX) && isNull(positionY) ) {
			zpl.append(ZplUtils.zplCommand("FT"));
		}
		return zpl.toString();
	}
}
