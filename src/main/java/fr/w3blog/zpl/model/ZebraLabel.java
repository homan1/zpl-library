package fr.w3blog.zpl.model;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import com.sun.javafx.binding.StringFormatter;
import fr.w3blog.zpl.constant.ZebraFont;
import fr.w3blog.zpl.constant.ZebraPrintMode;
import fr.w3blog.zpl.model.element.ZebraText;
import fr.w3blog.zpl.utils.ZplUtils;

import static java.util.Objects.isNull;

public class ZebraLabel {

	/**
	 * Width explain in dots
	 */
	private Integer widthDots;
	/**
	 * Height explain in dots
	 */
	private Integer heightDots;

	/**
	 * Label Home
	 * Sets the label home position
	 */
	private Integer homeXDots;
	private Integer homeYDots;

	/**
	 * Current Element Position
	 */
	private Integer currentPosX = 0;
	private Integer currentPosY = 0;
	private Integer previousPosX = 0;
	private Integer previousPosY = 0;

	/**
	 * Auto Adjust feature
	 */
	private Integer lineSpacing = 0;
	private boolean autoLineSpacing = false;
	private Integer gapLimit = 0;
	private Integer gap = 1;
	private boolean trackByLastElement = false;

    /**
     * Position
     */
    private boolean invertCoordinate = false;

	private ZebraPrintMode zebraPrintMode = ZebraPrintMode.TEAR_OFF;

	private PrinterOptions printerOptions = new PrinterOptions();

	private List<ZebraElement> zebraElements = new ArrayList<ZebraElement>();

	public ZebraLabel() {
		super();
		zebraElements = new ArrayList<ZebraElement>();
	}

	public ZebraLabel(PrinterOptions printerOptions) {
		super();
		this.printerOptions = printerOptions;
	}

	/**
	 * Create label with size
	 * 
	 * @param heightDots
	 *            height explain in dots
	 * @param widthDots
	 *            width explain in dots
	 */
	public ZebraLabel(int widthDots, int heightDots) {
		super();
		this.widthDots = widthDots;
		this.heightDots = heightDots;
	}

	/**
	 * 
	 * @param heightDots
	 *            height explain in dots
	 * @param widthDots
	 *            width explain in dots
	 * @param printerOptions
	 */
	public ZebraLabel(int widthDots, int heightDots, PrinterOptions printerOptions) {
		super();
		this.widthDots = widthDots;
		this.heightDots = heightDots;
		this.printerOptions = printerOptions;
	}

	/**
	 * Function to add Element on etiquette.
	 * 
	 * Element is abstract, you should use one of child Element( ZebraText, ZebraBarcode, etc)
	 * 
	 * @param zebraElement
	 * @return
	 */
	public ZebraLabel addElement(ZebraElement zebraElement) {


 		if( this.autoLineSpacing && zebraElement.getClass().equals(ZebraText.class)) {

			if(this.gap == 1) {
				if(trackByLastElement) {
					this.currentPosY += this.lineSpacing;
				} else {
					this.trackByLastElement = true;
				}


				zebraElement.setPositionX(this.currentPosX);
				zebraElement.setPositionY(this.currentPosY);

			}

			if (gap >= gapLimit) {
                    this.gap = 0;
            }

			gap++;
		}

		//Saves previous position for auto line spacing
		if(zebraElement.positionX!=null && zebraElement.positionY!=null) {
			this.previousPosX = currentPosX;
			this.previousPosY = currentPosY;
		}


		zebraElements.add(zebraElement);

		return this;
	}

	/**
	 * Use to define a default Zebra font on the label
	 * 
	 * @param defaultZebraFont
	 *            the defaultZebraFont to set
	 */
	public ZebraLabel setDefaultZebraFont(ZebraFont defaultZebraFont) {
		printerOptions.setDefaultZebraFont(defaultZebraFont);
		return this;
	}

	public void setLabelHome(Integer homeXDots, Integer homeYDots) {
		this.homeXDots = homeXDots;
		this.homeYDots = homeYDots;
	}

	public Integer getHomeXDots() {
		return homeXDots;
	}

	public void setHomeXDots(Integer homeXDots) {
		this.homeXDots = homeXDots;
	}

	public Integer getHomeYDots() {
		return homeYDots;
	}

	public void setHomeYDots(Integer homeYDots) {
		this.homeYDots = homeYDots;
	}

	public Integer getCurrentPosX() {
		return currentPosX;
	}

	public void setCurrentPosX(Integer currentPosX) {
		this.currentPosX = currentPosX;
	}

	public Integer getCurrentPosY() {
		return currentPosY;
	}

	public void setCurrentPosY(Integer currentPosY) {
		this.currentPosY = currentPosY;
	}

	public Integer getLineSpacing() {
		return lineSpacing;
	}

	public void setLineSpacing(Integer lineSpacing) {
		this.lineSpacing = lineSpacing;
	}

	public boolean isAutoLineSpacing() {
		return autoLineSpacing;
	}

	/**
	 * Auto positions elements. Set current pos X Y from last element
	 * @param lineSpacing Amount space between lines
	 * @param gapLimit Number of elements before it auto positions
	 */
	public void setAutoLineSpacing(Integer lineSpacing, Integer gapLimit) {
		this.autoLineSpacing = true;
		this.lineSpacing = lineSpacing;
		this.gapLimit = gapLimit;
		this.trackByLastElement = true;
		this.currentPosX = this.getZebraElements().get( this.getLastDataElement() ).getPositionX();
		this.currentPosY = this.getZebraElements().get( this.getLastDataElement() ).getPositionY();
	}

	/**
	 * Auto positions elements. Set current pos X Y from a set position
	 * @param lineSpacing Amount space between lines
	 * @param gapLimit Number of elements before it auto positions
	 * @param posX X coordinate of the last element
	 * @param posY Y coordinate of the last element
	 */
	public void setAutoLineSpacing(Integer lineSpacing, Integer gapLimit, Integer posX, Integer posY) {
		this.autoLineSpacing = true;
		this.lineSpacing = lineSpacing;
		this.gapLimit = gapLimit;
		this.trackByLastElement = false;
		this.currentPosX = posX;
		this.currentPosY = posY;
	}

	/**
	 * Turns off auto line spacing
	 * Resets gap
	 */
	public void setAutoLineSpacingOff() {
		this.autoLineSpacing = false;
		this.gap = 1;
		this.trackByLastElement = false;
	}

    /**
     * Get the position of the last data element
     * Works only for zebraText
     * @return
     */
	public int getLastDataElement() {
        for (int i = zebraElements.size()-1; i > 0; i--) {
            if(zebraElements.get(i).getClass().equals(ZebraText.class)) {
                return i;
            }
        }

        return -1;
    }

    public boolean isInvertCoordinate() {
        return invertCoordinate;
    }

    public void setInvertCoordinate(boolean invertCoordinate) {
        this.invertCoordinate = invertCoordinate;
    }

    public Integer getPreviousPosX() {
		return previousPosX;
	}

	public void setPreviousPosX(Integer previousPosX) {
		this.previousPosX = previousPosX;
	}

	public Integer getPreviousPosY() {
		return previousPosY;
	}

	public void setPreviousPosY(Integer previousPosY) {
		this.previousPosY = previousPosY;
	}

	public void setAutoLineSpacing(boolean autoLineSpacing) {
		this.autoLineSpacing = autoLineSpacing;
	}

	public Integer getGapLimit() {
		return gapLimit;
	}

	public void setGapLimit(Integer gapLimit) {
		this.gapLimit = gapLimit;
	}

	public Integer getGap() {
		return gap;
	}

	public void setGap(Integer gap) {
		this.gap = gap;
	}

	public boolean isTrackByLastElement() {
		return trackByLastElement;
	}

	public void setTrackByLastElement(boolean trackByLastElement) {
		this.trackByLastElement = trackByLastElement;
	}

	/**
	 * Use to define a default Zebra font size on the label (11,13,14).
	 * Not explain in dots (convertion is processed by library)
	 * 
	 * @param defaultFontSize
	 *            the defaultFontSize to set
	 */
	public ZebraLabel setDefaultFontSize(Integer defaultFontSize) {
		printerOptions.setDefaultFontSize(defaultFontSize);
		return this;
	}

	public Integer getWidthDots() {
		return widthDots;
	}

	public ZebraLabel setWidthDots(Integer widthDots) {
		this.widthDots = widthDots;
		return this;
	}

	public Integer getHeightDots() {
		return heightDots;
	}

	public ZebraLabel setHeightDots(Integer heightDots) {
		this.heightDots = heightDots;
		return this;
	}

	public PrinterOptions getPrinterOptions() {
		return printerOptions;
	}

	public void setPrinterOptions(PrinterOptions printerOptions) {
		this.printerOptions = printerOptions;
	}

	/**
	 * @return the zebraPrintMode
	 */
	public ZebraPrintMode getZebraPrintMode() {
		return zebraPrintMode;
	}

	/**
	 * @param zebraPrintMode
	 *            the zebraPrintMode to set
	 */
	public ZebraLabel setZebraPrintMode(ZebraPrintMode zebraPrintMode) {
		this.zebraPrintMode = zebraPrintMode;
		return this;
	}

	/**
	 * @return the zebraElements
	 */
	public List<ZebraElement> getZebraElements() {
		return zebraElements;
	}

	/**
	 * @param zebraElements
	 *            the zebraElements to set
	 */
	public void setZebraElements(List<ZebraElement> zebraElements) {
		this.zebraElements = zebraElements;
	}

	public String getZplCode() {
		StringBuilder zpl = new StringBuilder();

		zpl.append(ZplUtils.zplCommandSautLigne("XA"));//Start Label

        /**
         * default setting
         */
        zpl.append(ZplUtils.zplCommandSautLigne("PR12"));
        zpl.append(ZplUtils.zplCommandSautLigne("POI"));

        //Home location
		if (homeXDots != null && homeYDots != null) {
			zpl.append(ZplUtils.zplCommandSautLigne("LH", Integer.toString(homeXDots), Integer.toString(homeYDots)));
		}

		zpl.append(zebraPrintMode.getZplCode());

		//Label dimensions
		if (widthDots != null) {
			//Define width for label
			zpl.append(ZplUtils.zplCommandSautLigne("PW", widthDots));
		}

		if (heightDots != null) {
			zpl.append(ZplUtils.zplCommandSautLigne("LL", heightDots));
		}


		//Default Font and Size
//		if (printerOptions.getDefaultZebraFont() != null && printerOptions.getDefaultFontSize() != null) {
//			zpl.append(ZplUtils.zplCommandSautLigne("CF", (Object[]) ZplUtils.extractDotsFromFont(printerOptions.getDefaultZebraFont(), printerOptions.getDefaultFontSize(), printerOptions.getZebraPPP())));
//		}


        if (printerOptions.getDefaultZebraFont() != null && printerOptions.getDefaultFontSize() != null) {
			zpl.append(ZplUtils.zplCommandSautLigne("CF", (Object[]) ZplUtils.extractDotsFromFont(printerOptions.getDefaultZebraFont(), printerOptions.getDefaultFontSize(), printerOptions.getZebraPPP())));
		}


		for (ZebraElement zebraElement : zebraElements) {
			zpl.append(zebraElement.getZplCode(printerOptions));
		}
		zpl.append(ZplUtils.zplCommandSautLigne("XZ"));//End Label
		return zpl.toString();
	}

	public String getZplCode(boolean wrap) {

        StringBuilder zpl = new StringBuilder();

	    if(!wrap) {
            for (ZebraElement zebraElement : zebraElements) {
                zpl.append(zebraElement.getZplCode(printerOptions));
            }
        } else {
	        return getZplCode();
        }

        return zpl.toString();

    }

	/**
	 * Function use to have a preview of label rendering (not reflects reality).
	 * 
	 * Use it just to see disposition on label
	 * 
	 * @return Graphics2D
	 */
	public BufferedImage getImagePreview() {
		if (widthDots != null && heightDots != null) {
			int widthPx = ZplUtils.convertPointInPixel(widthDots);
			int heightPx = ZplUtils.convertPointInPixel(heightDots);
			BufferedImage image = new BufferedImage(widthPx, heightPx, BufferedImage.TYPE_INT_ARGB);
			Graphics2D graphic = image.createGraphics();
			graphic.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			graphic.setComposite(AlphaComposite.Src);
			graphic.fillRect(0, 0, widthPx, heightPx);

			graphic.setColor(Color.BLACK);
			graphic.setFont(new Font("Arial", Font.BOLD, 11));
			for (ZebraElement zebraElement : zebraElements) {
				zebraElement.drawPreviewGraphic(printerOptions, graphic);
			}
			return image;
		} else {
			throw new UnsupportedOperationException("Graphics Preview is only available ont label sized");
		}
	}
}
